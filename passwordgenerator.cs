/*
Made by Naomi Sinsheimer
Updated 10/01/20 
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Diagnostics;

namespace passwordgenerator
{

    public class passwordgenerator
    {

        //Words to use in password generation
        static string[] WORDS =
            {
                "Fairy",
                "Table",
                "Chair",
                "River",
                "Water",
                "Flower",
                "Thunder",
                "Theory",
                "Disappear",
                "Wonder",
                "Blanket",
                "Agree",
                "Oatmeal",
                "Hand",
                "Minute",
                "Pumpkin",
            };

        //Symbols to use in password generation
        static string[] SYMBOLS = 
            {
                "!", "@", "#", "$", "%", "?"
            };

        //Numbers to use in password generation
        static string[] NUMBERS = 
            {
                "1", "2", "3", "4", "5", "6", "7", "8", "9"
            };

        static int wordCount = WORDS.Length; //Counts elements in WORD array
        static int symbolCount = SYMBOLS.Length; //Counts elements in SYMBOL array
        static int numberCount = NUMBERS.Length; //Counts elements in NUMBERS array

        //Method that generates the passwords
        static string generatePassword(int i)
        {

            while(true)
            {

                string password, pw1, pw2, pw3, pw4;

                //Creates the Random object
                Random rand = new Random();
                int rnd1, rnd2, rnd3, rnd4;

                //Randomizes a number between 0 and however long the specified array is
                rnd1 = rand.Next(0, wordCount);
                rnd2 = rand.Next(0, numberCount);
                rnd3 = rand.Next(0, symbolCount);
                rnd4 = rand.Next(0, wordCount);

                //Assign string based on a random integer
                pw1 = WORDS[rnd1];
                pw2 = NUMBERS[rnd2];
                pw3 = SYMBOLS[rnd3];
                pw4 = WORDS[rnd4];

                //Concatenates to create the actual password
                password = pw1 + pw2 + pw3 + pw4;

                //Ensures the password is of a certain length
                if(password.Length >= 12 && password.Length <= 24)
                {
                    return password;
                }
                else
                {
                    continue;
                }
            }
        }
        
        //Method to get amount of passwords user needs
        static int userInput()
        {
            while (true)
            {
                Console.WriteLine("How many passwords do you want? (Max 30) ");
                string pwNumber = Console.ReadLine();

                var isNumeric = int.TryParse(pwNumber, out int n);

                //Checks that user input is an int and between 0-30
                if (isNumeric == true && n <= 30 && n != 0) 
                {
                    return n;
                }
                else
                //Prompts for input again if something invalid is entered.
                {
                    Console.WriteLine("Not a number or too large, try again.");
                    Thread.Sleep(1500);
                    Console.Clear();
                    continue;
                }
            }
        }

        public static void Main()
        {

            //Run UserInput method to get amount of passwords to generate
            int passwordammount = userInput(); 

            List<string> pwList = new List<string>();

            //Add generated passwords to a list
            for (int i = 0; i <= ((passwordammount)-1); i++)
            {

                pwList.Add(generatePassword(i));

            }

            TextWriter textwrite = new StreamWriter("passwords.txt");

            //Write list to a text file
            foreach (String s in pwList)
            {
                textwrite.WriteLine(s);
            }

            //Close open file and then open it with Notepad for easy copying
            textwrite.Close();
            Process.Start("notepad.exe", "passwords.txt");
            return;  
        }
    }
}